package com.example.socketapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException
import com.github.nkzawa.emitter.Emitter
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONArray


class MainActivity : AppCompatActivity() {
    private lateinit var mSocket: Socket
    private lateinit var trip : JSONObject
    init {
        //Todo: Hey !
        val options = IO.Options()
        val regCity = "5d714f9dc7f5550011292e50"
        val phone = "(940) 039-9387"
        options.query = "reg_city=$regCity&phone=$phone"
        //options.forceNew = true
        //options.reconnection = true
        //options.reconnectionDelay = 1000
        //options.reconnectionDelayMax = 10000
        //options.reconnectionAttempts = 9999

        try {
            mSocket = IO.socket("http://192.168.1.144:3050/client",options)
        } catch (e : URISyntaxException) {
            println(e)
        }

    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_emit.setOnClickListener { emit() }
        btn_disconnect.setOnClickListener { disconnect() }
        btn_reconnect.setOnClickListener { reconnect() }
        btn_getClosestDrivers.setOnClickListener { getClosestDrivers() }
        btn_newTripRequest.setOnClickListener { newTripRequest() }
        btn_cancelTrip.setOnClickListener { cancelTrip() }

        mSocket.on("connect", onConnection)
        mSocket.on("disconnect", onDisconnection)

        mSocket.on("getClosetDrivers", onGetClosestDrivers)
        mSocket.on("noDriverFound", onNoDriverFound)
        mSocket.on("updateTrip", updateTrip)


        mSocket.connect()
    }

    private val onConnection = Emitter.Listener { args ->
        this.runOnUiThread {
            Toast.makeText(this,"Connected",Toast.LENGTH_SHORT).show()
        }
    }
    private val onDisconnection = Emitter.Listener { args ->
        this.runOnUiThread {
            Toast.makeText(this,"Disconnected",Toast.LENGTH_SHORT).show()
        }
    }
    private val onGetClosestDrivers = Emitter.Listener { args ->
        this.runOnUiThread {
            Toast.makeText(this,"Get closest drivers !",Toast.LENGTH_SHORT).show()
        }
    }
    private val onNoDriverFound = Emitter.Listener { args ->
        this.runOnUiThread {
            Toast.makeText(this,"No driver found !",Toast.LENGTH_SHORT).show()
        }
    }

    private val updateTrip = Emitter.Listener { args ->
        trip = args[0] as JSONObject
        this.runOnUiThread {
            Toast.makeText(this,trip.getString("status"),Toast.LENGTH_SHORT).show()
        }
    }
    private fun emit() {
        mSocket.emit("getRooms")
    }
    private fun disconnect() {
        mSocket.disconnect()
    }

    private fun reconnect() {
        mSocket.open()
    }

    private fun cancelTrip() {
        val data = JSONObject()

        val location = JSONArray()
        location.put(-109.9328)
        location.put(27.496)
        try {
            data.put("id",trip.getString("_id"))
            data.put("city",trip.getString("city"))
            data.put("client_phone",trip.getString("client_phone"))

        } catch (e: JSONException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        mSocket.emit("cancel",data)
    }


    private fun getClosestDrivers() {
        val data = JSONObject()

        val location = JSONArray()
        location.put(-109.9328)
        location.put(27.496)
        try {
            data.put("city", "5d714f9dc7f5550011292e50")
            data.put("location",location)

        } catch (e: JSONException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        mSocket.emit("getClosetDriver",data)
    }

    private fun newTripRequest(){
        val data = JSONObject()
            try{
                data.put("estimated_price", 20)
                data.put("city", "5d714f9dc7f5550011292e50")
                val org = JSONArray()
                org.put(-110.9532204)
                org.put(29.1269356)
                data.put("org.virtual.coordinates",org)

            }catch (err: JSONException){

            }
        mSocket.emit("clientTripRequest",data)
    }
}
